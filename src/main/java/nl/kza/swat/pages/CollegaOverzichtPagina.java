package nl.kza.swat.pages;

import nl.kza.swat.basepages.Base;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class CollegaOverzichtPagina extends Base {

    @FindBy(css = "ul.users > li > a")
    private List<WebElement> _listAlleCollegas;
	
	public CollegaOverzichtPagina(WebDriver webDriver) {
		super(webDriver);
	}
	
	public CollegaDetailPagina GoToCollegaDetailPagina(String collega)
    {
        for (WebElement element : _listAlleCollegas)
        {
            if (getText(element).equals(collega))
            {
                clickOnElement(element);
                return new CollegaDetailPagina(webDriver);
            }
        }
        Assert.fail(errorMessages.get("GebruikerNietGevonden"));
        return new CollegaDetailPagina(webDriver);
    }
}
