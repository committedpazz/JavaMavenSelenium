package nl.kza.swat.pages;


import nl.kza.swat.basepages.Base;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Calendar;
import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class StartPaginaNaLogin extends Base {
	
	 @FindBy(id = "micropost_content") 		
	 private WebElement txtTypBericht;   
	 
	 @FindBy(css = "input.btn-primary[type='submit']")
     private WebElement _btnPlaatsBericht;   
	 
	 @FindBy(css = "ol.microposts > li")
     private List<WebElement> _listAlleBerichten;   	 
	 
	public StartPaginaNaLogin(WebDriver webDriver) {
		super(webDriver);
	}
	
	public StartPaginaNaLogin DoTypBericht(String bericht)
    {
        clearElement(txtTypBericht);
        sendKeys(txtTypBericht,bericht);
        return this;
    }

    public StartPaginaNaLogin DoPlaatsBericht()
    {
        _btnPlaatsBericht.click();
        return this;
        //Hier niet nodig
    }

    public StartPaginaNaLogin WithBericht(String gebruiker, String bericht)
    {
    	Calendar plaatsingsdatum = Calendar.getInstance();
        plaatsingsdatum.add(Calendar.HOUR, -1);
        String tijd = plaatsingsdatum.get(Calendar.HOUR_OF_DAY) + ":" + plaatsingsdatum.get(Calendar.MINUTE); //HH:MM
        String datum = plaatsingsdatum.get(Calendar.DAY_OF_MONTH) + ":" + (plaatsingsdatum.get(Calendar.MONTH)+1) + ":" + plaatsingsdatum.get(Calendar.YEAR);  //DD/MM/YYYY    		

        for (WebElement element : _listAlleBerichten)
        {
            if (getText(element.findElement(By.cssSelector("span.user > a"))).equals(gebruiker) &&
                getText(element.findElement(By.cssSelector("span.content"))).equals(bericht) &&
                getText(element.findElement(By.cssSelector("span.timestamp"))).contains(datum) &&
                getText(element.findElement(By.cssSelector("span.timestamp"))).contains(tijd))
            {
                return this;
            }
        }
        Assert.fail(errorMessages.get("BerichtNietGevonden"));
        return this;
    }

    public StartPaginaNaLogin VerwijderBericht(String gebruiker, String bericht)
    {
    	Calendar plaatsingsdatum = Calendar.getInstance();
        plaatsingsdatum.add(Calendar.HOUR, -1);
        String tijd = plaatsingsdatum.get(Calendar.HOUR_OF_DAY) + ":" + plaatsingsdatum.get(Calendar.MINUTE); //HH:MM
        String datum = plaatsingsdatum.get(Calendar.DAY_OF_MONTH) + ":" + (plaatsingsdatum.get(Calendar.MONTH)+1) + ":" + plaatsingsdatum.get(Calendar.YEAR);  //DD/MM/YYYY    		

        for (WebElement element : _listAlleBerichten)
        {
            if (getText(element.findElement(By.cssSelector("span.user > a"))).equals(gebruiker) &&
                getText(element.findElement(By.cssSelector("span.content"))).equals(bericht) &&
                getText(element.findElement(By.cssSelector("span.timestamp"))).contains(datum) &&
                getText(element.findElement(By.cssSelector("span.timestamp"))).contains(tijd))
            {
                clickOnElement(element.findElement(By.cssSelector("span.timestamp > a")));
                webDriver.switchTo().alert().accept();
                return this;
            }
        }
        Assert.fail(errorMessages.get("BerichtNietGevonden"));
        return this;
    }
}