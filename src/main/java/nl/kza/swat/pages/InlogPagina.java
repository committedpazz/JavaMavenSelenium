package nl.kza.swat.pages;


import nl.kza.swat.basepages.Base;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class InlogPagina extends Base {
	
    @FindBy(id = "session_email")
    private WebElement txtEmailAdres;

    @FindBy(id = "session_password")
    private WebElement txtWachtwoord;
    	
    @FindBy(className = "btn-primary") 		
    private WebElement btnLogIn;
    
	public InlogPagina(WebDriver webDriver) {
		super(webDriver);
	}
	
    public CollegaDetailPagina DoLogIn(String email, String wachtwoord)
    {
        sendKeys(txtEmailAdres,email);
        sendKeys(txtWachtwoord,wachtwoord);
        clickOnElement(btnLogIn);
        return new CollegaDetailPagina(webDriver);
    }
}