package nl.kza.swat.pages;

import nl.kza.swat.basepages.Base;
import nl.kza.swat.models.Messages;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static nl.kza.swat.models.Messages.errorMessages;

public class AanmeldPagina extends Base {
	
    @FindBy(id = "user_name")
    private WebElement txtNaam;

    @FindBy(id = "user_gender_man")
    private WebElement radiobtnGeslachtMan;
    
    @FindBy(id = "user_gender_vrouw")
    private WebElement radiobtnGeslachtVrouw;
    
    @FindBy(id = "user_residence")
    private WebElement txtWoonplaats;
    
    @FindBy(id = "user_dateofbirth")
    private WebElement txtGeboortedatum;
    
    @FindBy(id = "user_email")
    private WebElement txtEmailAdres;
    
    @FindBy(id = "user_phonenumber")
    private WebElement txtTelefoonnummer;
    
    @FindBy(id = "user_startdate")
    private WebElement txtIndienstTreding;
    
    @FindBy(id = "user_password")
    private WebElement txtWachtwoord;

    @FindBy(id = "user_password_confirmation")
    private WebElement txtWachtwoordBevestiging;
        
    @FindBy(className = "btn-primary")
    private WebElement btnMaakAccountAan;
    
	public AanmeldPagina(WebDriver webDriver) {
		super(webDriver);
	}
	
    public CollegaDetailPagina DoMaakAccountAan(String naam, String MV, String woonplaats, String geboortedatum, String email, String telefoonnummer, String indienst, String wachtwoord)
    {
        //first determine if we have valid input.
        if(!"M".equals(MV)&&!"V".equals(MV)){
            Assert.fail(errorMessages.get("Geslacht"));
        }

        //if we have valid input, then we can safely assume that if it's not a male, its a female.
        clickOnElement("M".equals(MV) ? radiobtnGeslachtMan : radiobtnGeslachtVrouw);
        sendKeys(txtNaam,naam);
        sendKeys(txtWoonplaats,woonplaats);
        sendKeys(txtGeboortedatum,geboortedatum);
        sendKeys(txtEmailAdres,email);
        sendKeys(txtTelefoonnummer,telefoonnummer);
        sendKeys(txtIndienstTreding,indienst);
        sendKeys(txtWachtwoord,wachtwoord);
        sendKeys(txtWachtwoordBevestiging,wachtwoord);
        clickOnElement(btnMaakAccountAan);

        return new CollegaDetailPagina(webDriver);
    }
}