package nl.kza.swat.pages;


import nl.kza.swat.basepages.HeaderFooter;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static nl.kza.swat.models.Messages.errorMessages;

public class CollegaDetailPagina extends HeaderFooter {
	
    @FindBy(tagName = "h1")
    private WebElement lblTitel;

    @FindBy(css	= "label.static-text[for='naam']")
    private WebElement lblNaam;
    
    @FindBy(css = "label.static-text[for='emailadres']")
    private WebElement lblEmail;

    @FindBy(css = "span.label-primary")
    private List<WebElement> listAlleBlauweLabeltjes;
        
    @FindBy(css = "table.table-condensed > tbody > tr > td")
    private List<WebElement> listAlleErvaringCellen;

    
	public CollegaDetailPagina(WebDriver webDriver) {
		super(webDriver);
	}
	
    public CollegaDetailPagina assertTitleIs (String titel)
    {
        Assert.assertEquals(errorMessages.get("GebruikerTitel") +  getText(lblTitel),titel,  getText(lblTitel));
        return this;
    }
    
    public CollegaDetailPagina WithNaam(String naam)
    {
        Assert.assertEquals(errorMessages.get("GebruikerNaam") +  getText(lblNaam), naam,  getText(lblNaam));
        return this;
    }

    public CollegaDetailPagina WithEmail(String email)
    {
        Assert.assertEquals(errorMessages.get("GebruikerEmail") +  getText(lblEmail), email,  getText(lblEmail) );
        return this;
    }
    
    public CollegaDetailPagina WithGroep(String groep)
    {
        if(blueLabelIsValid(groep,"Groepen",listAlleBlauweLabeltjes))
            return this;
        Assert.fail(errorMessages.get("GebruikerGroep"));
        return this;
    }
    
    public CollegaDetailPagina WithKennisgebied(String kennisgebied)
    {
        if(blueLabelIsValid(kennisgebied,"Kennisgebieden",listAlleBlauweLabeltjes))
            return this;
        Assert.fail(errorMessages.get("GebruikerKennisgebied"));
        return this;

    }
    
    public CollegaDetailPagina WithErvaring(String ervaring)
    {
        for (WebElement cel : listAlleErvaringCellen)
        {
            if (getText(cel).equals(ervaring))
                return this;
        }
        Assert.fail(errorMessages.get("GebruikerKlant"));
        return this;
    }

    public CollegaDetailPagina WithCursus(String cursus)
    {
        if(blueLabelIsValid(cursus,"Cursussen",listAlleBlauweLabeltjes))
            return this;
        Assert.fail(errorMessages.get("GebruikerCursus"));
        return this;
    }

    private boolean blueLabelIsValid(String containText,String elementText, List<WebElement> blueLabelList )
    {
        for (WebElement element : blueLabelList){
            if (getText(element).contains(containText))
            {
                WebElement blok = element
                        .findElement(By.xpath(".."))                           //parent
                        .findElement(By.xpath(".."))                           //parent
                        .findElement(By.xpath("preceding-sibling::*[1]"))      //sibling
                        .findElement(By.tagName("small"));
                if (getText(blok).contains(elementText))
                    return true;
            }
        }
        return false;
    }

}
