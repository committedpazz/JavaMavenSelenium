package nl.kza.swat.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import nl.kza.swat.models.AdminUser;
import nl.kza.swat.models.Customer;
import nl.kza.swat.models.User;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Wluijk on 7/18/2017.
 */
public class YamlLoader {

    private final Logger LOG = LoggerFactory.getLogger(YamlLoader.class);

    public YamlLoader() {
    }

    public User CreateUserFromYaml(String filename){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            User user = mapper.readValue(new File(TestConfig.valueFor("YamlDir")+filename+".yaml"), User.class);
            LOG.info(ReflectionToStringBuilder.toString(user, ToStringStyle.MULTI_LINE_STYLE));
            return user;
        } catch (Exception e) {
            LOG.error("YamlFile not loaded",e);
        }
        //return default values if something goes wrong
        LOG.warn("No userfile.yaml loaded, using default user");
        return new User();
    }

    public Customer CreateCustomerFromYaml(String filename){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            Customer customer = mapper.readValue(new File(TestConfig.valueFor("YamlDir")+filename+".yaml"), Customer.class);
            LOG.info(ReflectionToStringBuilder.toString(customer, ToStringStyle.MULTI_LINE_STYLE));
            return customer;
        } catch (Exception e) {
            LOG.error("YamlFile not loaded",e);
        }
        //return default values if something goes wrong
        LOG.warn("No customer.yaml loaded, using default customer");
        return new Customer();
    }

    public AdminUser CreateAdminUserFromYaml(String filename){
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            AdminUser adminUser = mapper.readValue(new File(TestConfig.valueFor("YamlDir")+filename+".yaml"), AdminUser.class);
            LOG.info(ReflectionToStringBuilder.toString(adminUser, ToStringStyle.MULTI_LINE_STYLE));
            return adminUser;
        } catch (Exception e) {
            LOG.error("YamlFile not loaded",e);
        }
        //return default values if something goes wrong
        LOG.warn("No customer.yaml loaded, using default customer");
        return new AdminUser();
    }

}
