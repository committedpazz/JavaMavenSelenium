// very slightly modified version of: https://github.com/cucumber/cucumber-jvm/blob/master/examples/java-webbit-websockets-selenium/src/test/java/cucumber/examples/java/websockets/SharedDriver.java

package nl.kza.swat.ui;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import java.util.concurrent.TimeUnit;
import static nl.kza.swat.ui.BrowserFactory.getBrowser;


public class SharedDriver extends EventFiringWebDriver {
    public static final WebDriver REAL_DRIVER;
    private static final Thread CLOSE_THREAD = new Thread() {
        @Override
        public void run() {
            REAL_DRIVER.close();
        }
    };
    private static String originalHandle;

    public static String getOriginalHandle() {
        return originalHandle;
    }

    public static void setOriginalHandle(String originalHandle) {
        SharedDriver.originalHandle = originalHandle;
    }

    static {
        Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
        try {
            REAL_DRIVER = getBrowser();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new Error(throwable);
        }
    }

    public SharedDriver() {
        super(REAL_DRIVER);
        REAL_DRIVER.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Override
    public void close() {
        if (Thread.currentThread() != CLOSE_THREAD) {
            throw new UnsupportedOperationException("You shouldn't close this WebDriver. It's shared and will close when the JVM exits.");
        }
        super.close();
    }


    public void deleteAllCookies() {
        manage().deleteAllCookies();
    }

    public WebDriver getBrowserInstance(){
        return REAL_DRIVER;
    }

}
