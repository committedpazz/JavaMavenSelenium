package nl.kza.swat.ui;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import nl.kza.swat.util.TestConfig;

import java.util.concurrent.TimeUnit;

class ChromeBrowser extends ChromeDriver {
    public static WebDriver buildChromeBrowser() throws Throwable {
    	System.setProperty("webdriver.chrome.driver", TestConfig.valueFor("WebDriverChromeDriverPath"));
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
    	ChromeBrowser browser = new ChromeBrowser(options);
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return browser;
    }
    
    private ChromeBrowser(ChromeOptions options) {
    	super(options);
    }
}
