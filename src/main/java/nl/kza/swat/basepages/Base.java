package nl.kza.swat.basepages;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.junit.Assert.assertTrue;
import static nl.kza.swat.ui.SharedDriver.getOriginalHandle;

public class Base {
    public WebDriver webDriver;
    private final Logger LOG = LoggerFactory.getLogger(Base.class);
    private int retryCount = 0;

    protected Base(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    private void takeScreenshot(WebDriver driver){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("target/screenshots/"+"screenshot_"+timeStamp+ ".jpg"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public WebDriver getDriver() {
        return webDriver;
    }

    public void closeExtraTabs(){
        for(String handle : webDriver.getWindowHandles()) {
            if (!handle.equals(getOriginalHandle())) {
                webDriver.switchTo().window(handle);
                webDriver.close();
            }
        }
        webDriver.switchTo().window(getOriginalHandle());
    }


    public void clickOnElement(WebElement element){
        LOG.debug("[m] clickOnElement | click element " + element);

        if(isElementPresent(element))
            waitForElementToBeClickable(element).click();

        //manual reset of retry timer just in case it messes up the instances
        if(retryCount!=0){
            retryCount=0;
        }
    }

    public void clearElement(WebElement element){
        LOG.debug("[m] clearElement | clear field " + element);
        if(isElementPresent(element)){
            element.clear();
        } else {
            LOG.warn("[m] clearElement | element not found, start waiting for element, screenshot has been taken");
            takeScreenshot(webDriver);
            waitForElement(element);
            element.clear();
        }

    }

    public void sendKeys(WebElement element, String keyText){
        LOG.debug("[m] sendKeys | fill field with value " + keyText);
        if(isElementPresent(element)){
            element.sendKeys(keyText);
        } else {
            LOG.warn("[m] sendKeys | element not found, start waiting for element, screenshot has been taken");
            takeScreenshot(webDriver);
            waitAndSendKeys(element,keyText);
        }
    }

    public String getText(WebElement element){
        LOG.debug("[m] getText | get text from element " + element);
        if(isElementPresent(element)){
            LOG.debug(("[m] getText | textfound:  " + element.getText()));
            return element.getText();
        } else {
            LOG.warn("[m] getText | element not found, retrying for 20 seconds");
            takeScreenshot(webDriver);
            waitForElement(element);
            LOG.debug(("[m] getText | textfound:  " + element.getText()));
            return element.getText();
        }
    }

    public boolean textFromElementIsEqual(WebElement element, String compareValue){
        LOG.debug("[m] compareText | Compare " + element + " with compare value " + compareValue );
        return getText(element).equals(compareValue);
    }



    public boolean isElementPresent(WebElement element) {
        LOG.debug("[m] isElementPresent | validate if element exist: "+element);

        try {
            element.getTagName();
            return true;
        } catch (NoSuchElementException e) {
            ++retryCount;
            LOG.error("[m] isElementPresent | No such element found screenshot has been taken, starting to wait for element \n retrycount:" + retryCount,e);
            takeScreenshot(webDriver);
            if(retryCount!=1) {
                waitForElement(element);
                isElementPresent(element);
            }
            return false;
        }
    }

    public void scrollToElement(WebElement el) {
        if (getDriver() instanceof JavascriptExecutor) {
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", el);
        }
    }

    public void scrollToElement2(WebElement el) {
        if (getDriver() instanceof JavascriptExecutor) {

            Point p = el.getLocation();
            System.out.println(((JavascriptExecutor) getDriver()).executeScript("window.scroll(" + p.getX() + "," + (p.getY() + 200) + ");"));
            ((JavascriptExecutor) getDriver()).executeScript("window.scroll(" + p.getX() + "," + (p.getY() + 200) + ");");

            waitExplicitly(2000);
        }
    }

    public void waitForElementOrScrollToIt(WebElement el) {
        try {
            waitForElementToBeClickable(el);
            el.click();
        } catch (WebDriverException e) {
            LOG.error("element not clickable",e);
            scrollToElement(el);
            waitForElementToBeClickable(el);
        }

    }

    public void waitAndClick(WebElement el) {
        try {
            waitForElementToBeClickable(el);
            el.click();
        } catch (WebDriverException e) {
            LOG.error("element not clickable",e);
            scrollToElement(el);
            waitForElementToBeClickable(el);
            el.click();
        }
    }

    public void waitAndClickBy(By by) {
        WebElement el = getDriver().findElement(by);
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), 10, 500);
            wait.until(ExpectedConditions.elementToBeClickable(by));
            el.click();
        } catch (WebDriverException e) {
            LOG.error("unexpected condition",e);
            scrollToElement(el);
            waitForElement(el);
            el.click();
        }
    }

    public void waitAndSendKeys(WebElement el, String text) {
        LOG.debug("[m] waitAndSendKeys | waiting for element and sending string to element");
        waitForElement(el);
        el.sendKeys(text);
    }

    public void waitForElement(WebElement el) {
        LOG.debug("[m] waitForElement | Start to wait for element for 20 seconds ");
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.visibilityOf(el));
    }

    public void waitForElementNotPresent(WebElement el) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10, 500);
        wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(el)));
    }

    protected void waitForElements(List<WebElement> findElements) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 10, 500);
        if (!findElements.isEmpty()) {
            wait.until(ExpectedConditions.elementToBeClickable(findElements.get(0)));
        }
    }

    public void waitTillElementIsInvisible(By by) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public void waitTillElementIsVisible(By by) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitTillElementIsVisible(WebElement el) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.visibilityOf(el));
    }

    public void waitTillElementsAreVisible(List<WebElement> el) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.visibilityOfAllElements(el));
    }

    public WebElement waitForElementToBeClickable(WebElement el) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.elementToBeClickable(el));
        return el;
    }

    public void waitForFrameAndSwitchToIt(String frameLocator) {
        WebDriverWait wait = new WebDriverWait(getDriver(), 20, 500);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
    }

    public void waitExplicitly(int millis) {
        try {
            Thread.sleep(millis);
        } catch (final InterruptedException e) {
            LOG.error("Explicit wait interrupted", e);
        }
    }

    public WebElement waitForStaleElement(WebElement element, By by) {

        int attempts = 0;
        while (attempts < 2) {

            try {
                element = getDriver().findElement(by);
                break;
            } catch (StaleElementReferenceException e) {
            }

            attempts++;
        }

        return element;
    }

    public List<WebElement> waitForStaleElements(List<WebElement> elements, By by) {

        int attempts = 0;
        while (attempts < 2) {

            try {
                elements = getDriver().findElements(by);
                break;
            } catch (StaleElementReferenceException e) {
            }

            attempts++;
        }

        return elements;
    }

	/* Switch to POP-UP
    public boolean switchToPopUp() throws Throwable {

		// By by_wicketmodal = By.className("wicket_modal");
		boolean status = false;

		while (status == false) {
			Thread.sleep(Constant.MicroWait);
			wait = new WebDriverWait(data.getDriver(), 10);
			// wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(driver.findElement(by_wicketmodal)));
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(0));
			status = true;

			break;
		}
		return status;
	}

	// Switch to POP-UP in POP-UP
	public boolean switchToPopUp2() throws Throwable {

		data.getDriver().switchTo().defaultContent();

		boolean status = false;

		while (status == false) {
			wait = new WebDriverWait(data.getDriver(), 10);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(1));
			status = true;

			break;
		}
		return status;
	}

	// Switch to POP-UP in POP-UP in POP-UP
	public boolean switchToPopUp3() throws Throwable {

		data.getDriver().switchTo().defaultContent();

		boolean status = false;

		while (status == false) {
			wait = new WebDriverWait(data.getDriver(), 10);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(2));
			status = true;

			break;
		}
		return status;
	}

	// Switch to Page
	public boolean switchToPage() throws Throwable {

		boolean status = false;

		try {
			data.getDriver().switchTo().defaultContent();
			Thread.sleep(Constant.ShortWait);
			status = true;

		} catch (Exception e) {
			System.out.println("failed to switch to default page");
		}

		return status;
	}*/

    // Wait for Attribute to contain Value
    public boolean assertAttributeValue(By by, String attribute, String equals) {
        boolean elementIsEqual = false;
        int attempts = 0;
        String elementValue;
        while (attempts < 10 || elementIsEqual) {
            try {
                Thread.sleep(100);
                elementValue = getDriver().findElement(by).getAttribute(attribute);
                LOG.debug("[m] assertAttributeValue | attribute is: " + attribute);
                LOG.debug("[m] assertAttributeValue | equal value is: " + equals);
                LOG.debug("[m] assertAttributeValue | elementValue value is: " + elementValue);
                elementIsEqual=elementValue.equals(equals);
            } catch (Exception e) {
                if (attempts == 9) {
                    elementValue = getDriver().findElement(by).getAttribute(attribute);
                    LOG.error( "[m] assertAttributeValue | failed after #" + (attempts + 1) + " attempts! current value: " + elementValue
                            + " | expected value: " + equals);
                }

            }
            attempts++;
        }

        return elementIsEqual;
    }

}

	