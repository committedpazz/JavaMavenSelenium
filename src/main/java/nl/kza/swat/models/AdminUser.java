package nl.kza.swat.models;

/**
 * Created by Wluijk on 7/19/2017.
 */
public class AdminUser {
    private String name;
    private String email;
    private String password;

    public AdminUser(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public AdminUser() {
       this("TestUser","usertest@test.nl","XYEJ2233AA");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
